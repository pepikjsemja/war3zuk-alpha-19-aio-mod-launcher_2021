  _  ___              _____
 | |/ (_)            / ____|
 | ' / _ _ __   __ _| |  __  ___ _ __
 |  < | | '_ \ / _` | | |_ |/ _ \ '_ \
 | . \| | | | | (_| | |__| |  __/ | | |
 |_|\_\_|_| |_|\__, |\_____|\___|_| |_|
                __/ |
               |___/
v0.13.1 - A19.6
7 Days to Die Random World Generator
Find out more at https://community.7daystodie.com/topic/23988-kinggen-a-random-world-generator-for-7-days-to-die/

SETTINGS:
border_biome=wasteland
border_size=medium
border_type=water
burnt_size=small
check_output=false
cities_grid_size=medium
cities_level=60
cities_number=lots
cities_size=large
default_biome=snow
default_biome_tools=pine forest
desert_size=small
heightmap=cannibal_coast_cpt_krunch_8k.png
heightmap_smoothing=medium
mountains_size=none
multiple_generations=1
name=War3zuk Official Map 8k
output_folder=
png_heightmap=no
poi_mode=false
pois_list=C:/KingGen/poislist.txt
pois_number=increased
seed=War3zuk Official Map 8k
size=8
skip_preview=false
snow_level=
snow_size=large
spacing=medium
spawn_points=increased
terrain_roughness=none
towns_grid_size=medium
towns_number=medium
towns_size=large
traders_location=cities
traders_number=medium
villages_grid_size=medium
villages_number=lots
villages_size=medium
waste_size=small
water_level=43
water_size=none